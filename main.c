
#include "main.h"

int main(char **argv, int argc) {
    test_tree_struct();
    return 0;
}




void test_tree_struct() {
    int del_arr[] = {1, 3, 6, 4, 2};//, 5, 7, 8};
    for (int d = 0; d < countof(del_arr); d++) {
        int arr[] = {2, 1, 7, 3, 8, 4, 6, 5};
        tree *test = NULL;
        for (int i = 0; i < countof(arr); i++) {
            insert_tree(&test, arr[i], NULL);
        }
        printf("\nInit tree\n");
        traverse_tree(test);
        printf("\ndelete %d in tree\n", del_arr[d]);
        delete_tree(&test, del_arr[d]);
        traverse_tree(test);
        free(test);
    }
}

