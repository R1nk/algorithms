//
// Created by r1nk on 23.11.18.
//

#ifndef ALGORYTHMS_SORTS_H
#define ALGORYTHMS_SORTS_H
#define ITEM int

/**
 * swap left and righ items
 * @param left ITEM
 * @param right ITEM
 */
void swap(ITEM *left, ITEM *right);

/**
 *  O(n*n)
 * @param s array of ITEM
 * @param n array length
 */
void insertion_sort(ITEM s[], int n);

/**
 * O(n*n)
 * @param s array of ITEM
 * @param n array length
 */
void selection_sort(ITEM s[], int n);

#endif //ALGORYTHMS_SORTS_H
