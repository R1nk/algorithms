//
// Created by r1nk on 23.11.18.
//
#include "SortMethods.h"

void swap(ITEM *left, ITEM *right) {
    ITEM *tmp = left;
    left = right;
    right = tmp;
}


void insertion_sort(ITEM s[], int n) {
    int i, j;
    for (i = 1; i < n; i++) {
        j = 1;
        while ((j > 0) && (s[j] < s[j - 1])) {
            swap(&s[j], &s[j - 1]);
            j = j - 1;
        }
    }
}

void selection_sort(ITEM s[], int n) {
    int i, j;
    int min;
    for (i = 0; i < n; i++) {
        min = i;
        for (j = i + 1; j < n; j++) {
            if (s[j] < s[min])
                min = j;
            swap(&s[i], &s[min]);
        }
    }
}