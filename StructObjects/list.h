//
// Created by r1nk on 28.11.18.
//
#include <stdio.h>
#include <stdlib.h>
#ifndef ALGORYTHMS_LIST_H
#define ALGORYTHMS_LIST_H
#define ITEM int


typedef struct list {
    ITEM item;
    struct list *next;
} list;

list *search_list(list *l, ITEM x);
void *insert_list(list **l, ITEM x);
list *predecessor_list(list *l, ITEM x);
void *delete_list(list **l, ITEM x);

#endif //ALGORYTHMS_LIST_H
