//
// Created by r1nk on 28.11.18.
//

#include "list.h"


list *search_list(list *l, ITEM x) {
    if (l == NULL) return NULL;
    if (l->item == x) return l;
    else return search_list(l->next, x);
}

void *insert_list(list **l, ITEM x) {
    list *p;
    p = malloc(sizeof(list));
    p->item = x;
    p->next = *l;
    *l = p;
}

list *predecessor_list(list *l, ITEM x) {
    if ((l == NULL) || (l->next == NULL)) {
        printf("Error: predecessor sought on null list. \n");
        return NULL;
    }
    if ((l->next)->item == x)
        return l;
    else return predecessor_list(l->next, x);
}

void *delete_list(list **l, ITEM x) {
    list *p;
    list *pred;
    p = search_list(*l, x);
    if (p != NULL) {
        pred = predecessor_list(*l, x);
        if (pred == NULL) *l = p->next;
        else pred->next = p->next;
        free(p);
    }
}
