//
// Created by r1nk on 28.11.18.
//
#include <stdio.h>
#include <stdlib.h>

#ifndef ALGORYTHMS_TREE_H
#define ALGORYTHMS_TREE_H
#define ITEM int
typedef struct tree{
    ITEM item;
    struct tree *parent;
    struct tree *left;
    struct tree *right;
} tree;

tree *search_tree(tree *l, ITEM x);
tree *find_minimum_tree(tree *l);
tree *find_maximum_tree(tree *l);
void traverse_tree(tree *l);
void insert_tree(tree **l,ITEM x,tree *parent);
void delete_tree(tree **l, ITEM x);

#endif //ALGORYTHMS_TREE_H
