//
// Created by r1nk on 28.11.18.
//
#include "tree.h"

tree *search_tree(tree *l, ITEM x) {
    if (l == NULL) return NULL;
    if (l->item == x)return l;
    if (x < l->item) return search_tree(l->left, x);
    else return search_tree(l->right, x);
}

tree *find_minimum_tree(tree *l) {
    tree *min;
    if (l == NULL) return NULL;
    min = l;
    while (min->left != NULL)
        min = min->left;
    return min;
}

tree *find_maximum_tree(tree *l) {
    tree *min;
    if (l == NULL) return NULL;
    min = l;
    while (min->right != NULL)
        min = min->right;
    return min;
}

void traverse_tree(tree *l) {
    if (l != NULL) {
        traverse_tree(l->left);
        ///show item in console

        printf("%d", l->item);
        if (l->parent != NULL)
            printf(" -> parent - %d\n", l->parent->item);
        else
            printf(" -> root\n");

        traverse_tree(l->right);
    }
}

void insert_tree(tree **l, ITEM x, tree *parent) {
    tree *p;
    if (*l == NULL) {
        p = malloc(sizeof(tree));
        p->item = x;
        p->left = p->right = NULL;
        p->parent = parent;
        *l = p;
        return; ///warning inf recursion
    }
    if (x < (*l)->item)
        insert_tree(&((*l)->left), x, *l);
    else
        insert_tree(&((*l)->right), x, *l);
}

void delete_tree(tree **l, ITEM x) {
    tree *del = *l;
    ///if tree is empty then nothing to do
    if (del != NULL) {
        ///find item in tree
        if (x != del->item) {
            if (x < del->item)
                delete_tree(&del->left, x);
            if (x > del->item)
                delete_tree(&del->right, x);
        } else {
            ///get child for replace to deleted item
            tree *temp = del->left == NULL ? find_minimum_tree(del->right) : del->left;

            ///check childs
            if (temp != NULL) {
                ///except inf loop----------------------------------
                if (temp != del->left)
                    temp->left = del->left;
                if (temp != del->right)
                    temp->right = del->right;
                ///when move node need null parent pointer to node
                temp->parent->left = NULL;
                ///set parent for child nodes
                if (del->left != NULL)
                    del->left->parent = temp;
                if (del->right != NULL)
                    del->right->parent = temp;
                if (del->parent != NULL)
                    if (del->item < del->parent->item)
                        del->parent->left = temp;
                    else
                        del->parent->right = temp;
                else
                    temp->parent = NULL;
            }
            if (del->parent == NULL)
                *l = temp;
            free(del);
        }
    }
}